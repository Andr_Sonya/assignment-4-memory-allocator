#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define DEFAULT_HEAP_SIZE 1024

void debug(const char *fmt, ...);

void *mmap_pages(void const *address, size_t len, int flags)
{
	return mmap((void *)address, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | flags, -1, 0);
}
#define get_header(mem) \
   ((struct block_header *)(((uint8_t *)(mem)) - offsetof(struct block_header, contents)))

void test_heap_init()
{
	void *h = heap_init(DEFAULT_HEAP_SIZE);
	assert(h);
	heap_term();
	debug("[test_heap_init]       \033[92m[OK]\033[39m\n");
}

void test_free_one()
{
	void *h = heap_init(DEFAULT_HEAP_SIZE);
	assert(h);

	void *bolck1 = _malloc(150);
	void *block2 = _malloc(300);
	void *block3 = _malloc(450);

	assert(bolck1);
	assert(block2);
	assert(block3);

	_free(block2);
	assert(!get_header(bolck1)->is_free);
	assert(get_header(block2)->is_free);
	assert(!get_header(block3)->is_free);

	_free(bolck1);
	_free(block3);
	heap_term();
	debug("[test_free_one]        \033[92m[OK]\033[39m\n");
}

void test_free_many()
{
	void *heap = heap_init(DEFAULT_HEAP_SIZE);
	assert(heap);

	void *block1 = _malloc(150);
	void *block2 = _malloc(300);
	void *block3 = _malloc(450);

	assert(block1);
	assert(block2);
	assert(block3);

	_free(block1);
	_free(block3);

	assert(get_header(block1)->is_free);
	assert(!get_header(block2)->is_free);
	assert(get_header(block3)->is_free);

	_free(block2);

	heap_term();
	debug("[test_free_many]       \033[92m[OK]\033[39m\n");
}


void test_region_exp() {

	struct region* h = heap_init(0);
	assert(h);

	size_t initial_region_size = h->size;

	_malloc(10 * DEFAULT_HEAP_SIZE);
	size_t expanded_region_size = h->size;


	assert(initial_region_size < expanded_region_size);


	heap_term();
	debug("[test_region_exp]      \033[92m[OK]\033[39m\n");
}

void test_region_exp_new() {
	void* region = mmap_pages(HEAP_START, 128, MAP_FIXED);
	assert(region);

	void* block = _malloc(512);
	assert(block);
	assert(region != block);

	heap_term();
	debug("[test_region_exp_new]  \033[92m[OK]\033[39m\n");
}
int main()
{
	test_heap_init();
	test_free_one();
	test_free_many();
	test_region_exp();
	test_region_exp_new();
}
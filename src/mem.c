#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);
extern inline bool region_is_invalid(const struct region *r);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
	*((struct block_header *)addr) = (struct block_header) {
		.next = next,
			.capacity = capacity_from_size(block_sz),
			.is_free = true
	};
}

static size_t region_actual_size(size_t query)
{
	return size_max(round_pages(query), REGION_MIN_SIZE);
}

static void *map_pages(void const *addr, size_t length, int additional_flags)
{
	return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query)
{
	const size_t actual_size = region_actual_size(size_from_capacity((block_capacity) { .bytes = query }).bytes);

	void *region_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);
	if (region_addr == MAP_FAILED)
	{
		if ((region_addr = map_pages(addr, actual_size, 0)) == MAP_FAILED)
			return REGION_INVALID;
	}

	block_init(region_addr, (block_size) { .bytes = actual_size }, NULL);
	return (struct region) {
		.addr = region_addr,
			.size = actual_size,
			.extends = (region_addr == addr),
	};
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query)
{
	return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *current_block, size_t requested_size)
{
	if (!current_block || !block_splittable(current_block, requested_size))
		return false;

	requested_size = size_max(requested_size, BLOCK_MIN_CAPACITY);

	const block_capacity requested_capacity = (block_capacity) { .bytes = requested_size };
	const block_size requested_block_size = size_from_capacity(requested_capacity);

	const block_capacity remaining_capacity = (block_capacity) {
		.bytes = current_block->capacity.bytes - requested_block_size.bytes,
	};

	const block_size remaining_block_size = size_from_capacity(remaining_capacity);

	block_init(
		(void *)current_block + requested_block_size.bytes,
		remaining_block_size,
		current_block->next);

	current_block->capacity = requested_capacity;
	current_block->next = (void *)((unsigned char *)current_block + requested_block_size.bytes);
	return true;
}

/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block)
{
	return (void *)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
	struct block_header const *fst,
	struct block_header const *snd)
{
	return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
	return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *current_block)
{
	const struct block_header *next_block = current_block->next;

	if (!current_block || !next_block)
		return false;
	if (!mergeable(current_block, next_block))
		return false;

	block_size merged_size = (block_size) {
		.bytes = size_from_capacity(current_block->capacity).bytes + size_from_capacity(next_block->capacity).bytes
	};

	block_init(current_block, merged_size, next_block->next);
	return true;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result
{
	enum
	{
		BSR_FOUND_GOOD_BLOCK,
		BSR_REACHED_END_NOT_FOUND,
		BSR_CORRUPTED
	} type;
	struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict current_block, size_t requested_size)
{
	if (!current_block)
		return (struct block_search_result) { .type = BSR_CORRUPTED };

	while (current_block) {
		if (current_block->is_free) {
			while (try_merge_with_next(current_block))
				;

			if (block_is_big_enough(requested_size, current_block)) {
				return (struct block_search_result) { .block = current_block, .type = BSR_FOUND_GOOD_BLOCK };
			}
		}

		if (current_block->next == NULL)
			break;

		current_block = current_block->next;
	}

	return (struct block_search_result) { .block = current_block, .type = BSR_REACHED_END_NOT_FOUND };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t requested_size, struct block_header *current_block)
{
	struct block_search_result result = find_good_or_last(current_block, requested_size);

	if (result.type == BSR_CORRUPTED || result.type == BSR_REACHED_END_NOT_FOUND) {
		return result;
	}

	split_if_too_big(result.block, requested_size);
	result.block->is_free = false;

	return result;
}

static struct block_header *grow_heap(struct block_header *restrict last_block, size_t requested_size)
{
	if (last_block == NULL)
		return NULL;

	struct region new_heap_region = alloc_region(block_after(last_block), requested_size);
	if (region_is_invalid(&new_heap_region))
		return NULL;

	last_block->next = new_heap_region.addr;

	if (new_heap_region.extends && try_merge_with_next(last_block))
		return last_block;
	return last_block->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t requested_size, struct block_header *heap_start)
{
	if (heap_start == NULL)
		return NULL;

	requested_size = size_max(requested_size, BLOCK_MIN_CAPACITY);
	struct block_search_result allocation_result = try_memalloc_existing(requested_size, heap_start);

	if (allocation_result.type == BSR_CORRUPTED)
		return NULL;
	if (allocation_result.type == BSR_FOUND_GOOD_BLOCK)
		return allocation_result.block;

	struct block_header *new_heap_region = grow_heap(
		allocation_result.block,
		size_from_capacity((block_capacity) { .bytes = requested_size }).bytes);

	if (!new_heap_region)
		return NULL;

	return try_memalloc_existing(requested_size, new_heap_region).block;
}

void *_malloc(size_t query)
{
	struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
	if (addr)
		return addr->contents;
	else
		return NULL;
}

void *heap_init(size_t initial)
{
	const struct region region = alloc_region(HEAP_START, initial);
	if (region_is_invalid(&region))
		return NULL;

	return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term()
{
	struct block_header *block = (struct block_header*) HEAP_START;
    struct block_header *next;
    while(block) {
        size_t current_size = size_from_capacity(block->capacity).bytes;
        next = block -> next;
        while (next && blocks_continuous(block, next)) {
            current_size += size_from_capacity(next->capacity).bytes;
            next = next -> next;
        }
        munmap(block, current_size);
        block = next; 
    }
}

static struct block_header *block_get_header(void *contents)
{
	return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem)
{
	if (!mem)
		return;
	struct block_header *header = block_get_header(mem);
	header->is_free = true;
	while (try_merge_with_next(header))
		;
}
